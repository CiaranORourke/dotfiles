return {
    -- add gruvbox
    {
        "sainnhe/gruvbox-material",
        lazy = false,
        priority = 1000,
    },

    {
        "LazyVim/LazyVim",
        opts = {
            colorscheme = "gruvbox-material",
        },
    },
}
